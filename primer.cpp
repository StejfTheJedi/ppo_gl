#define CATCH_CONFIG_MAIN
#include <iostream>
#include "lib/catch.hpp"
#include "Izracun.cpp"

TEST_CASE( "Factorials are computed", "[factorial]" ) {
    Izracun i;
    REQUIRE( i.Calc(0) == 1 ); // Komentiraj, da ni napake.
    REQUIRE( i.Calc(1) == 1 );
    REQUIRE( i.Calc(2) == 2 );
    REQUIRE( i.Calc(3) == 6 );
    REQUIRE( i.Calc(10) == 3628800 );
}