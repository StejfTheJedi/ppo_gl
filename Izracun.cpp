#include <iostream>
using namespace std;

class Izracun {
    private:
        int rezultat;
    public:
        // Izracun();
        Izracun();
        unsigned int Calc( unsigned int number );
};

Izracun::Izracun(){
    rezultat = 0;
}

/*Izracun::Izracun(){
    rezultat = 0;
}*/

unsigned int Izracun::Calc( unsigned int number ) {
    return number <= 1 ? number : Calc(number-1)*number;
}