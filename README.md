**Kratka navodila**

Zadnjih 6 poslov zajema vse željene primere.

Integracija z GitLabCI na dveh področjih:
- build
- test
    
Ukazi se najdejo v .yaml datoteki. Pri testiranju je bil uporabljen catch.

**Izracun.cpp**: razred, ki ima implementirano metodo za izračun fakultete.

**main.cpp**: uporaba razreda Izracun.cpp

**primer.cpp**: testni primer za razred Izracun.cpp

Pomembno pri build:
 - Build posel propade, če cppcheck odkrije napako. (Možen hiter preizkus: v Izracun.cpp priporocam, da se zakomentira privzeti konstruktor)
 - Artefakti: izvršljiva datoteka, ki pride iz main.cpp; napake v ustreznih tekstovnih datotekah. Težava: nisem našel pametnega načina, kak bi izpisal napake v datoteke, preden bi v primeru napake posel bil neuspešno končan.
 
 Pomembno pri test:
 - V datoteki primer.cpp je testni primer, ki preverja logiko. V primeru, da test spodleti je posel neuspešen (možen preizkus: priporočam, da se odkomentira prva vrstica v primer.cpp).
 - Artefakti: tekstovna datoteka, ki kaže rezultat testiranja (težava: v primeru neuspeha se posel zaključi prej, kot se kaj zapiše v datoteko).